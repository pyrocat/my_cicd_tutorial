# pull official base image
FROM python:3.10-slim-bullseye as builder

SHELL ["/bin/bash", "-c"]

# set environment variables
ENV SECRET_KEY="" \
    DEBUG=""\
    DJANGO_ALLOWED_HOSTS=""\
    CELERY_BROKER=""\
    CELERY_BACKEND=""

EXPOSE 8000

RUN useradd -Um -u 1000 --shell /bin/bash hc_user && chmod 755 /opt /run

# set working directory
WORKDIR /opt/app

RUN mkdir /opt/app/static &&  \
    mkdir /opt/app/media &&  \
    mkdir /opt/app/logs && \
    mkdir /opt/app/local_db && \
    chown -R hc_user:hc_user /opt/app

# copy project
COPY --chown=hc_user:hc_user project /opt/app

# install dependencies
RUN pip install --upgrade pip
COPY requirements.txt /opt/app/requirements.txt
RUN pip install -r requirements.txt

USER hc_user

ENTRYPOINT ["/app/entrypoint.sh"]

FROM builder as app
ENTRYPOINT python manage.py runserver 0.0.0.0:8000

FROM builder as worker

ENV WORKER_QUEUE = "celery"
ENTRYPOINT celery -A main worker --loglevel=info -Q $WORKER_QUEUE --events

FROM builder as dashboard

ENTRYPOINT celery flower -A main --port=5555 --broker=$CELERY_BROKER

